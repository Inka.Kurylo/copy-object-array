'use strict';
const user = {
    bioParams: [160, 50],
    name: 'Inna',
    lastname: 'Ku',
    age: 30,
    adress: {
        str: 'Polit',
        house: 10,
    },
};
function copy (obj){
    let newObj = {};
    for (let key in obj){
        if (Array.isArray(obj[key])){
            newObj[key] = obj[key].slice();
        } else if (typeof obj[key] === 'object'){
            newObj[key] = copy(obj[key]);
        }  else 
            newObj[key] = obj[key];
        }
    return newObj;
};
console.log (copy(user));

